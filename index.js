//Librerías
const express = require('express')
const app = express()
const https = require('https')
const http = require('http').Server(app)
const io = require('socket.io')(http, https)
const net = require('net')
const cookieParser = require('cookie-parser')
const parser = require('body-parser')
const moment = require('moment')
const fs = require('fs')
//Certificado para HTTPS
const privateKey  = fs.readFileSync('./private.key', 'utf8');
const certificate = fs.readFileSync('./certificate.crt', 'utf8');
//Archivo de configuración
const config = require('./config.js')

//Configuración del servidor Express
app.use(express.static(__dirname + '/Public'))
app.use(parser())
app.use(cookieParser())
app.use(express.json())

app.get('/', function(req, res) {
  var mensaje = req.query.msg
  io.sockets.emit('msg', mensaje)
  res.send('MSG recibido: ' + mensaje)
})

app.get('/:msg', function(req, res) {
  var mensaje = req.params.msg
  io.sockets.emit('msg', mensaje)
  res.send('MSG recibido: ' + mensaje)
})

//SocketIO (Tiempo Real)
io.on('connection', function(socket) {
  socket.on('disconnect', function () {
    console.log('Cliente desconectado:', socket.id)
  })
})

//Servidor TCP
var tcp = net.createServer(onClientConnected)
tcp.listen(config.tcp, '0.0.0.0', function() {
  console.log('Servidor TCP montado *:' + config.tcp)//%j', tcp.address())
})

function onClientConnected(sock) {
  sock.on('data', function(data) {
    var payload = String(data)
    io.sockets.emit('msg', payload)
    sock.write('MSG recibido: ');
    sock.write(data);
  });
};


//Server HTTPS (Puerto 443 por defecto)
var credentials = {key: privateKey, cert: certificate};
var httpsServer = https.createServer(credentials, app).listen(config.httpsPort, function() {
  console.log('Servidor HTTPS montado *:' + config.httpsPort)
});

//Server HTTP (Puerto 80 por defecto)
http.listen(config.httpPort, function() {
  console.log('Servidor HTTP montado *:' + config.httpPort)
})
